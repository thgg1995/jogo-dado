package jogodado;

import java.util.List;

public class Resultado {
//	private int valores;
//	
//	public Resultado(int valores) {
//		this.valores = valores;
//	}
	
	public static int somar(List<Integer> valores) {
		int resultado = 0;
		
		for(int valor : valores) {
			resultado = resultado + valor;
		}
		return resultado;
	}
}
