package jogodado;

public class Dado {
	private int lados;

	public Dado(int lados) {
		this.lados = lados;
	}

	public int sortear() {
		return (int) Math.ceil((Math.random() * lados));
	}
	
}
