package jogodado;

import java.util.ArrayList;
import java.util.List;

public class Sorteador {
	private static List<Integer> listaValores = new ArrayList<Integer>();
	private static String textoRetorno = "";


	public static String Sortear(Dado dado, int numeroJogadas) {

		for (int i = 0; i < numeroJogadas; i++) {
			listaValores.add(dado.sortear());
		}

		return FormatarTexto(Resultado.somar(listaValores));

	}

	private static String FormatarTexto(int resultado) {
		for (int i = 0; i < listaValores.size(); i++) {
			if (i != listaValores.size() - 1)
				textoRetorno = textoRetorno + listaValores.get(i) + ",";
			else
				textoRetorno = textoRetorno + listaValores.get(i);
		}
		textoRetorno = textoRetorno + "-" + resultado;
		return textoRetorno;
	}
}
